import cv2
import os
import numpy as np
import glob

eigenfaces = cv2.face.EigenFaceRecognizer_create()
numeroTeste = 10 #numero referente a quantidade de testes
pctBase = 100 #definição da porcentagem utilizada da base

def getImagemComId():
    #caminhos = [os.path.join('treinamento/imagens/100/1/conhecidos/**/*.pgm', f) for f in os.listdir('treinamento/imagens/100/1/conhecidos/**/*.pgm')]

    faces = []
    ids = []
    for caminhoTreinamento in glob.glob(os.path.join('treinamento/imagens/'+ str(pctBase) +'/' + str(numeroTeste) + '/conhecidos/**/*.pgm')):

        imagemFace = cv2.cvtColor(cv2.imread(caminhoTreinamento), cv2.COLOR_BGR2GRAY)
        id = int(os.path.split(caminhoTreinamento)[1].split("_")[0].split("B")[1])
        ids.append(id)
        faces.append(imagemFace)

    return np.array(ids), faces

        #cv2.imshow("Teste", imagemFace)
        #cv2.waitKey(10)


ids, faces = getImagemComId()
print(ids)

print("treinando...")
eigenfaces.train(faces, ids)
eigenfaces.write('treinamento/descritores/opencv/'+str(pctBase)+ '/' + str(numeroTeste) +'/descritoresEigenFaces.yml')


