import matplotlib.pyplot as plt
import numpy as np

# false positive rate
tpr = [0,51, 54, 55, 55,55]
# true positive rate
fpr = [0,20, 7, 10, 11, 22]

tpr = sorted(tpr)
fpr = sorted(fpr)

tpr = [0, 54, 55, 55, 51, 55]
fpr = [0, 7, 10, 11, 20, 22]

print(tpr)
print(fpr)

# false positive rate
tpr2 = [0, 59, 88, 95, 100]
# true positive rate
fpr2 = [0, 2, 21, 41, 100]


plt.title(' ROC - Receiver Operating Characteristic')
plt.xlabel('1 - Especificidade')
plt.ylabel('Sensibilidade')
plt.plot(fpr,tpr,label="Base completa", marker='.')
plt.plot(fpr2,tpr2,label="Base completa", marker='.')

plt.show()
