**Dlib Face Recognition**
[Original Code](https://github.com/davisking/dlib)

---

## INSTALA��O
---

## Requisitos

1. [Anaconda](https://www.anaconda.com/)
2. Python 3.6
3. Dlib 
4. Numpy
5. OpenCV
6. Pickle

---
## Passos

1. Instalar Anaconda Navigator
2. Criar um ambiente virtual
3. Instalar as bibliotecas necess�rias (Dlib, Python, OpenCV, Pickle)
4. Utilizar qualquer IDE do Anaconda.
5. Efetuar download das imagens do dataset [Extended Yale Face Database B+](http://vision.ucsd.edu/~iskwak/ExtYaleDatabase/ExtYaleB.html)
6. enjoy the code

---

## METODOLOGIA
Esse reposit�rio comporta um total de 50 testes. Cada teste � um subconjunto do anterior, sendo o primeiro conjunto com 100% da base e o restante
com 70%, 50%, 40% e 25% das imagens com as melhores condi��es de ilumina��o. Os testes realizados por este trabalho � uma deriva��o com
a base atualizada do trabalho de [(ELDER, 2016)](https://repositorio.ufsc.br/handle/123456789/166102). Cada teste consiste com um total de 28 pessoas, cada um deles possuem 24 pessoas conhecidas e 4 pessoas desconhecidas.
Para cada conjunto foram realizados 10 testes, alternando as pessoas conhecidas e desconhecidas aleatoriamente.
---
## BATERIA DE TESTES

As imagens testadas nos conjuntos(100%, 70%, 50%, 40% e 25% da base Yale B+) podem ser visualiadas abaixo:

###TESTE 01
#####Pessoas procuradas: ['yaleB16', 'yaleB17', 'yaleB18', 'yaleB19', 'yaleB20', 'yaleB21', 'yaleB22', 'yaleB23', 'yaleB24', 'yaleB25', 'yaleB26', 'yaleB27', 'yaleB28', 'yaleB29', 'yaleB30', 'yaleB31', 'yaleB32', 'yaleB33', 'yaleB34', 'yaleB35', 'yaleB36', 'yaleB37', 'yaleB38', 'yaleB39'] 
#####Pessoas desconhecidas: ['yaleB11', 'yaleB12', 'yaleB13', 'yaleB15']

###TESTE 02
#####Pessoas procuradas: ['yaleB11', 'yaleB12', 'yaleB13', 'yaleB15', 'yaleB20', 'yaleB21', 'yaleB22', 'yaleB23', 'yaleB24', 'yaleB25', 'yaleB26', 'yaleB27', 'yaleB28', 'yaleB29', 'yaleB30', 'yaleB31', 'yaleB32', 'yaleB33', 'yaleB34', 'yaleB35', 'yaleB36', 'yaleB37', 'yaleB38', 'yaleB39'] 
#####Pessoas desconhecidas: ['yaleB16', 'yaleB17', 'yaleB18', 'yaleB19'] 

###TESTE 03:
#####Pessoas procuradas: ['yaleB11', 'yaleB12', 'yaleB13', 'yaleB15', 'yaleB16', 'yaleB17', 'yaleB18', 'yaleB19', 'yaleB24', 'yaleB25', 'yaleB26', 'yaleB27', 'yaleB28', 'yaleB29', 'yaleB30', 'yaleB31', 'yaleB32', 'yaleB33', 'yaleB34', 'yaleB35', 'yaleB36', 'yaleB37', 'yaleB38', 'yaleB39'] 
#####Pessoas desconhecidas: ['yaleB20', 'yaleB21', 'yaleB22', 'yaleB23'] 

###TESTE 04:
#####Pessoas procuradas: ['yaleB11', 'yaleB12', 'yaleB13', 'yaleB15', 'yaleB16', 'yaleB17', 'yaleB18', 'yaleB19', 'yaleB20', 'yaleB21', 'yaleB22', 'yaleB23', 'yaleB28', 'yaleB29', 'yaleB30', 'yaleB31', 'yaleB32', 'yaleB33', 'yaleB34', 'yaleB35', 'yaleB36', 'yaleB37', 'yaleB38', 'yaleB39'] 
#####Pessoas desconhecidas: ['yaleB24', 'yaleB25', 'yaleB26', 'yaleB27'] 

###TESTE 05:
#####Pessoas procuradas: ['yaleB11', 'yaleB12', 'yaleB13', 'yaleB15', 'yaleB16', 'yaleB17', 'yaleB18', 'yaleB19', 'yaleB20', 'yaleB21', 'yaleB22', 'yaleB23', 'yaleB24', 'yaleB25', 'yaleB26', 'yaleB27', 'yaleB32', 'yaleB33', 'yaleB34', 'yaleB35', 'yaleB36', 'yaleB37', 'yaleB38', 'yaleB39'] 
#####Pessoas desconhecidas: ['yaleB28', 'yaleB29', 'yaleB30', 'yaleB31'] 

###TESTE 06:
#####Pessoas procuradas: ['yaleB11', 'yaleB12', 'yaleB13', 'yaleB15', 'yaleB16', 'yaleB17', 'yaleB18', 'yaleB19', 'yaleB20', 'yaleB21', 'yaleB22', 'yaleB23', 'yaleB24', 'yaleB25', 'yaleB26', 'yaleB27', 'yaleB28', 'yaleB29', 'yaleB30', 'yaleB31', 'yaleB36', 'yaleB37', 'yaleB38', 'yaleB39'] 
#####Pessoas desconhecidas: ['yaleB32', 'yaleB33', 'yaleB34', 'yaleB35'] 

###TESTE 07:
#####Pessoas procuradas: ['yaleB11', 'yaleB12', 'yaleB13', 'yaleB15', 'yaleB16', 'yaleB17', 'yaleB18', 'yaleB19', 'yaleB20', 'yaleB21', 'yaleB22', 'yaleB23', 'yaleB24', 'yaleB25', 'yaleB26', 'yaleB27', 'yaleB28', 'yaleB29', 'yaleB30', 'yaleB31', 'yaleB32', 'yaleB33', 'yaleB34', 'yaleB35'] 
#####Pessoas desconhecidas: ['yaleB36', 'yaleB37', 'yaleB38', 'yaleB39'] 

###TESTE 08:
#####Pessoas procuradas: ['yaleB12', 'yaleB13', 'yaleB15', 'yaleB16', 'yaleB17', 'yaleB18', 'yaleB20', 'yaleB21', 'yaleB22', 'yaleB23', 'yaleB24', 'yaleB26', 'yaleB27', 'yaleB28', 'yaleB29', 'yaleB30', 'yaleB32', 'yaleB33', 'yaleB34', 'yaleB35', 'yaleB36', 'yaleB37', 'yaleB38', 'yaleB39'] 
#####Pessoas desconhecidas: ['yaleB11', 'yaleB19', 'yaleB25', 'yaleB31'] 

###TESTE 09:
#####Pessoas procuradas: ['yaleB11', 'yaleB12', 'yaleB13', 'yaleB15', 'yaleB16', 'yaleB17', 'yaleB19', 'yaleB20', 'yaleB21', 'yaleB22', 'yaleB24', 'yaleB25', 'yaleB26', 'yaleB27', 'yaleB29', 'yaleB30', 'yaleB31', 'yaleB32', 'yaleB34', 'yaleB35', 'yaleB36', 'yaleB37', 'yaleB38', 'yaleB39'] 
#####Pessoas desconhecidas: ['yaleB18', 'yaleB23', 'yaleB28', 'yaleB33'] 

###TESTE 10:
#####Pessoas procuradas: ['yaleB11', 'yaleB12', 'yaleB13', 'yaleB16', 'yaleB18', 'yaleB19', 'yaleB20', 'yaleB21', 'yaleB22', 'yaleB23', 'yaleB24', 'yaleB26', 'yaleB27', 'yaleB28', 'yaleB30', 'yaleB31', 'yaleB32', 'yaleB33', 'yaleB34', 'yaleB35', 'yaleB36', 'yaleB37', 'yaleB38', 'yaleB39'] 
#####Pessoas desconhecidas: ['yaleB15', 'yaleB17', 'yaleB25', 'yaleB29'] 