import cv2
import glob
import os
import datetime
from datetime import date

numeroTeste = 1 #numero referente a quantidade de testes
pctBase = 25 #definição da porcentagem utilizada da base


for rodada in range(10):

    idsConhecidos = []
    idsDesconhecidos = []
    pessoasDesconhecidas = []
    pessoasConhecidas = []
    nome = ' '
    totalFaces = 0
    facesReconhecidas = 0
    faceNaoReconhecida = 0
    listaFacesReconhecidas = []
    totalImagens = 0
    listaVP = []
    listaVN = []
    listaFP = []
    listaFN = []
    start = datetime.datetime.now()
    for allPeople in glob.glob(os.path.join('treinamento/imagens/'+ str(pctBase) +'/' + str(numeroTeste) + '/conhecidos/**/*.pgm')):

        pessoasConhecidas.append(allPeople.split("\\")[2].split("_")[0])
        idsConhecidos.append(int(os.path.split(allPeople)[1].split("_")[0].split("B")[1]))

    print("Faces conhecidas da rodada:")
    pessoasConhecidas = sorted(set(pessoasConhecidas))
    print(pessoasConhecidas)
    print(idsConhecidos)

    for allPeopleD in glob.glob(os.path.join('treinamento/imagens/'+ str(pctBase) + '/' + str(numeroTeste) + '/desconhecidos/**/*.pgm')):

        pessoasDesconhecidas.append(allPeopleD.split("\\")[2].split("_")[0])
        idsDesconhecidos.append(int(os.path.split(allPeopleD)[1].split("_")[0].split("B")[1]))

    print("Faces desconhecidas da rodada:")
    pessoasDesconhecidas = sorted(set(pessoasDesconhecidas))
    print(pessoasDesconhecidas)
    print(idsDesconhecidos)

    detectorFace = cv2.CascadeClassifier("recursos/haarcascade_frontalface_default.xml")
    reconhecimentoFacial = cv2.face.EigenFaceRecognizer_create()
    reconhecimentoFacial.read("treinamento/descritores/opencv/"+ str(pctBase) +"/" + str(numeroTeste) + "/descritoresEigenFaces.yml")
    font = cv2.FONT_HERSHEY_COMPLEX_SMALL


    for arquivo in glob.glob(os.path.join('yalefaces' + str(pctBase) + '/**/*.pgm')):

        totalImagens += 1
        detectVP = False
        detectVN = False
        detectFP = False
        detectFN = False
        id = None

        imagem = cv2.cvtColor(cv2.imread(arquivo), cv2.COLOR_BGR2GRAY)
        facesDetectadas = detectorFace.detectMultiScale(imagem, scaleFactor=1.5, minSize=(100,100))
        totalFaces += len(facesDetectadas)

        faceAtual = arquivo.split("\\")[2].split("_")[0]
        idAtual = (int(os.path.split(arquivo)[1].split("_")[0].split("B")[1]))
        print("Analisando pessoa: {}".format(faceAtual))

        for (x, y, l, a) in facesDetectadas:

            id, confianca = reconhecimentoFacial.predict(imagem)
            print("FACE ID ATUAL {} RECONHECIDA COMO ID {} CONFIANCA DE {}".format(idAtual, id, confianca))
            facesReconhecidas += 1

            if id != None:

                if idAtual == id:

                    detectVP = True
                    print("FACE CLASSIFICADA COMO VERDADEIRO POSITIVO {}".format(arquivo.split("\\")[2].split(".")[0]))
                    listaVP.append(arquivo.split("\\")[2].split(".")[0])
                    break

                else:

                    detectFP = True
                    print("FACE CLASSIFICADA COMO FALSO POSITIVO {}".format(arquivo.split("\\")[2]))
                    listaFP.append(arquivo.split("\\")[2].split(".")[0])
                    break

            else:

                if idAtual in idsConhecidos:
                    detectFN = True
                    print("*************\n*****************\n")
                    print("*************\n*****************\n")
                    print("FACE CLASSIFICADA COMO FALSO NEGATIVO {}".format(arquivo.split("\\")[2]))
                    listaFN.append(arquivo.split("\\")[2].split(".")[0])
                    #exclude.append(arquivo)
                    break

                elif idAtual in idsDesconhecidos:

                    detectVN = True
                    print("*************\n*****************\n")
                    print("*************\n*****************\n")
                    print("FACE CLASSIFICADA COMO VERDADEIRO NEGATIVO {}".format(arquivo.split("\\")[1]))
                    listaVN.append(arquivo.split("\\")[2].split(".")[0])
                    break


            #cv2.rectangle(imagem, (x, y), (x + l, y + a), (255,255,255), 2)
            #print("A face {} foi reconhecida como {} \n id Atual {}".format(faceAtual, id, idAtual))
            #cv2.putText(imagem, str(id), (x, y + (a + 20)), font, 2, (255,255,255))

        if idAtual in idsConhecidos and detectVP == False and detectVN == False and detectFP == False and detectFN == False:
            print("FACE NÃO DETECTADA CLASSIFICADA COMO FALSO NEGATIVO {}".format(arquivo.split("\\")[2]))
            listaFN.append(arquivo.split("\\")[2].split(".")[0])
            #exclude.append(arquivo)

        elif idAtual in idsDesconhecidos and detectVP == False and detectVN == False and detectFP == False and detectFN == False:
            print("FACE NÃO DETECTADA CLASSIFICADA COMO VERDADEIRO NEGATIVO {}".format(arquivo.split("\\")[2]))
            listaVN.append(arquivo.split("\\")[2].split(".")[0])


        #cv2.imshow("PCA face", imagem)
        #if cv2.waitKey(1) == ord('q'):
        #    break


    listaFN = sorted((set(listaFN)))
    listaVP = sorted((set(listaVP)))
    listaVN = sorted((set(listaVN)))
    listaFP = sorted((set(listaFP)))

    accuracy = (len(listaVP) + len(listaVN)) / (len(listaVP) + len(listaFP) + len(listaVN) + len(listaFN))
    recall = len(listaVP) / (len(listaVP) + len(listaFN))
    specificity = len(listaVN) / (len(listaFP) + len(listaVN))
    precision = len(listaVP) / (len(listaVP) + len(listaFP))
    fpr = len(listaFP) / (len(listaFP) + len(listaVN))
    fscore = 2 * ((precision * recall) / (precision + recall))

    print("____________________________________________\n")

    print("Total de imagens analisadas: {}".format(totalImagens))
    print("Total de faces detectadas: {}".format(totalFaces))
    print("Total de faces reconhecidas: {}".format(facesReconhecidas))
    print("Acuracia: {}".format(accuracy))
    print("Precisao: {}".format(precision))
    print("Sensibilidade: {}".format(recall))
    print("Especificidade: {}".format(specificity))
    print("False Positive Rate: {}".format(fpr))
    print("F-score: {}".format(fscore))

    print("____________________________________________\n")

    with open('resultados/' + str(pctBase) + '/' + str(numeroTeste) + '/teste_PCA_'+ str(pctBase) + '_0' + str(numeroTeste) + '.txt', 'w', encoding="utf-8") as results:

        results.write("Executado em: {} \n".format(date.today()))
        results.write("Pessoas procuradas: {} \n".format(pessoasConhecidas))

        results.write("Pessoas desconhecidas: {} \n".format(pessoasDesconhecidas))

        results.write("Total de Imagens: {}".format(totalImagens))
        results.write("\n")

        results.write("Total de faces detectadas: {}".format(totalFaces))
        results.write("\n")

        results.write("Total de faces reconhecidas: {}".format(facesReconhecidas))
        results.write("\n")

        results.write("\n")
        results.write("\n")
        results.write("Lista VP [{}]: {}".format(len(listaVP), listaVP))
        results.write("\n")

        results.write("Lista VN [{}]: {}".format(len(listaVN), listaVN))
        results.write("\n")

        results.write("Lista FP [{}]: {}".format(len(listaFP), listaFP))
        results.write("\n")

        results.write("Lista FN [{}]: {}".format(len(listaFN), listaFN))
        results.write("\n")
        results.write("\n")

        results.write("Matrix Confusion Results\n")
        results.write("Acuracia: {}\n".format(accuracy))
        results.write("Precisao: {}\n".format(precision))
        results.write("Sensibilidade: {}\n".format(recall))
        results.write("Especificidade: {}\n".format(specificity))
        results.write("False Positive Rate: {}\n".format(fpr))
        results.write("F-score: {}\n".format(fscore))

        end = datetime.datetime.now()

        results.write("Tempo de execução: \n {}".format(end - start))

    results.close
    cv2.destroyAllWindows()
    numeroTeste += 1
