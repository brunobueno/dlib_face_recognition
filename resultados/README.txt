Pasta destinada a guardar os resultados dos testes.

100: Teste com 100% das faces da Extended Yale Face Database B (B+).
70: Teste com 70% das faces da Extended Yale Face Database B (B+).
50: Teste com 50% das faces da Extended Yale Face Database B (B+).
40: Teste com 40% das faces da Extended Yale Face Database B (B+).
25: Teste com 25% das faces da Extended Yale Face Database B (B+).