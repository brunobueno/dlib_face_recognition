import os
import glob
import dlib
import cv2
import numpy as np
import datetime
from datetime import date

# def imprimeNumeros(imagem, pontosFaciais):
#     for i, p in enumerate(pontosFaciais.parts()):
#         cv2.putText(imagem, str(i), (p.x, p.y), cv2.FONT_HERSHEY_COMPLEX_SMALL, .55, (220,20,60), 1)

numeroTeste = 2 #numero referente a quantidade de testes
pctBase = 25 #definição da porcentagem utilizada da base

for rodada in range(10):

    print("Iniciando rodada {}".format(numeroTeste))
    detectorFace = dlib.get_frontal_face_detector() #metodo para detectar faces com bounding boxes
    #detectorFace = dlib.cnn_face_detection_model_v1("recursos/mmod_human_face_detector.dat")
    detectorPontosFaciais = dlib.shape_predictor("recursos/shape_predictor_68_face_landmarks.dat") #metodo para detectar os 68 pontos faciais do rosto humano, passando como parametro o arquivo treinando em HOG e SVM
    reconhecimentoFacial = dlib.face_recognition_model_v1("recursos/dlib_face_recognition_resnet_model_v1.dat") #treinamento feito especificamente para reconhecimento facial com rede convolucional
    indices = np.load("treinamento/indices/" + str(pctBase) + "/" + str(numeroTeste) + "/indices.pickle") #carregar indices
    descritoresFaciais = np.load("treinamento/descritores/"+ str(pctBase) +"/" + str(numeroTeste) + "/descritores.npy") #carregar nossa base de dados de treinamento
    limiar = 0.6 #quando uma face tiver a distancia menor que o limiar entao vou buscar la na minha base de dados e vou classificar com o rotulo correspondente
    #0.6 pq é a recomendação que está na bilblioteca do DLIB
    nome = ' '
    totalFaces = 0
    facesReconhecidas = 0
    faceNaoReconhecida = 0
    listaFacesReconhecidas = []
    pessoasConhecidas = []
    pessoasDesconhecidas = []
    totalImagens = 0
    listaVP = []
    listaVN = []
    listaFP = []
    listaFN = []
    start = datetime.datetime.now()


    for allPeople in glob.glob(os.path.join('treinamento/imagens/'+ str(pctBase) +'/' + str(numeroTeste) + '/conhecidos/**/*.pgm')):

        pessoasConhecidas.append(allPeople.split("\\")[2].split("_")[0])

    print("Faces conhecidas da rodada:")
    pessoasConhecidas = sorted(set(pessoasConhecidas))
    print(pessoasConhecidas)

    for allPeopleD in glob.glob(os.path.join('treinamento/imagens/'+ str(pctBase) + '/' + str(numeroTeste) + '/desconhecidos/**/*.pgm')):

        pessoasDesconhecidas.append(allPeopleD.split("\\")[2].split("_")[0])

    print("Faces desconhecidas da rodada:")
    pessoasDesconhecidas = sorted(set(pessoasDesconhecidas))
    print(pessoasDesconhecidas)

    #for arquivo in glob.glob(os.path.join('fotos/lfw-deepfunneled/teste/**/*.jpg')):
    for arquivo in glob.glob(os.path.join('yalefaces' + str(pctBase) + '/**/*.pgm')):


        totalImagens += 1
        imagem = cv2.imread(arquivo) #para ler a imagem
        detectVP = False
        detectVN = False
        detectFP = False
        detectFN = False

        print("____________________________________________\n")
        faceAtual = arquivo.split("\\")[2].split("_")[0]

        print("Analisando pessoa: {}".format(faceAtual))

        facesDetectadas = detectorFace(imagem, 1) #p criar BBox e aumentar a escala em 2 por conta que temos imagens muito pequenas, ai aumentamos para aumentar a precisão tbm do reconhecimento
        print("Posição da face {} ".format(facesDetectadas))
        totalFaces += len(facesDetectadas)
        # janela = dlib.image_window()
        # janela.set_image(detectorFace)
        # dlib.hit_enter_to_continue()

        for face in facesDetectadas:
            e, t, d, b = (int(face.left()), int(face.top()), int(face.right()), int(face.bottom()))
            pontosFaciais = detectorPontosFaciais(imagem, face) #por meio dos pontos faciais da imagem corrente é que vamos mandar para o classificador (rede neural) e só vai analisar dentro da face (Bounding box
            #print(pontosFaciais)
            descritorFacial = reconhecimentoFacial.compute_face_descriptor(imagem, pontosFaciais) #encontrar quais são as melhores caracteristicas dessa imagem

            #print("descritores faciais do banco {}.".format(descritoresFaciais))
            #aqui em baixo usar o processo bem parecido com o treinamento, pois precisamos manter o mesmo padrão de comparação -----

            listaDescritorFacial = [df for df in descritorFacial]
            #print(len(listaDescritorFacial))
            npArrayDescritorFacial = np.asarray(listaDescritorFacial, dtype=np.float64)
            npArrayDescritorFacial = npArrayDescritorFacial[np.newaxis, :]
            #print(npArrayDescritorFacial)

            distancias = np.linalg.norm(npArrayDescritorFacial - descritoresFaciais, axis=1)
            #print("descritores faciais da entrada {}.".format(descritorFacial))
            #aqui é aplicado a distância euclediana, comparando o que temos no arquivo de treinamento(base) com o que temos de entrada
            #axis=1 pq temos a coluna 0 e 1, porém os valores estão na coluna 1
            #print("distancias {}".format(distancias))

            minimo = np.argmin(distancias)
            distanciaMinima = distancias[minimo]
            #print(distanciaMinima, len(facesDetectadas))

            nome = os.path.split(indices[minimo])[1].split("_")[0]  # split = quebra a string

            if distanciaMinima <= limiar:

                facesReconhecidas += 1
                listaFacesReconhecidas.append("A face {}".format(arquivo.split("\\")[2].split(".")[0]) + " foi identificada como {}".format(nome))
                print("A face {}".format(arquivo.split("\\")[2].split(".")[0]) + " foi identificada como {}".format(nome))

                if faceAtual == nome:
                    detectVP = True
                    print("FACE CLASSIFICADA COMO VERDADEIRO POSITIVO {}".format(arquivo.split("\\")[2].split(".")[0]))
                    listaVP.append(arquivo.split("\\")[2].split(".")[0])
                    break

                else:
                    detectFP = True
                    print("FACE CLASSIFICADA COMO FALSO POSITIVO {}".format(arquivo.split("\\")[2]))
                    listaFP.append(arquivo.split("\\")[2].split(".")[0])
                    break

            else:

                if faceAtual in pessoasConhecidas:
                    detectFN = True
                    print("FACE CLASSIFICADA COMO FALSO NEGATIVO {}".format(arquivo.split("\\")[2]))
                    listaFN.append(arquivo.split("\\")[2].split(".")[0])
                    #exclude.append(arquivo)
                    break

                elif faceAtual in pessoasDesconhecidas:

                    detectVN = True
                    print("FACE CLASSIFICADA COMO VERDADEIRO NEGATIVO {}".format(arquivo.split("\\")[1]))
                    listaVN.append(arquivo.split("\\")[2].split(".")[0])
                    break

                nome = ' '



        cv2.rectangle(imagem, (e, t), (d, b), (255,255,255), 2)
        #texto = "{} ".format(nome)
        #cv2.putText(imagem, texto, (e + 6, t - 6), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.5, (0, 255, 255))


        cv2.imshow("Detector", imagem)
        key = cv2.waitKey(0)
        if key == 27:  # if ESC is pressed, exit loop
            cv2.destroyAllWindows()
            break

        #método para resgatar todas as faces da pessoa conhecida, mas que não foi detectada a Bbox, consequentemente, sem reconhecimento
        #sendo classificada como FALSO NEGATIVA

        if faceAtual in pessoasConhecidas and detectVP == False and detectVN == False and detectFP == False and detectFN == False:
            print("FACE NÃO DETECTADA CLASSIFICADA COMO FALSO NEGATIVO {}".format(arquivo.split("\\")[2]))
            listaFN.append(arquivo.split("\\")[2].split(".")[0])
            #exclude.append(arquivo)

        elif faceAtual in pessoasDesconhecidas and detectVP == False and detectVN == False and detectFP == False and detectFN == False:
            print("FACE NÃO DETECTADA CLASSIFICADA COMO VERDADEIRO NEGATIVO {}".format(arquivo.split("\\")[2]))
            listaVN.append(arquivo.split("\\")[2].split(".")[0])


    listaFN = sorted((set(listaFN)))
    listaVP = sorted((set(listaVP)))
    listaVN = sorted((set(listaVN)))
    listaFP = sorted((set(listaFP)))

    accuracy = (len(listaVP) + len(listaVN)) / (len(listaVP) + len(listaFP) + len(listaVN) + len(listaFN))
    recall = len(listaVP) / (len(listaVP) + len(listaFN))
    specificity = len(listaVN) / (len(listaFP) + len(listaVN))
    precision = len(listaVP) / (len(listaVP) + len(listaFP))
    fpr = len(listaFP) / (len(listaFP) + len(listaVN))
    fscore = 2 * ((precision * recall) / (precision + recall))

    print("____________________________________________\n")

    print("Total de imagens analisadas: {}".format(totalImagens))
    print("Total de faces detectadas: {}".format(totalFaces))
    print("Total de faces reconhecidas: {}".format(facesReconhecidas))
    print("Acuracia: {}".format(accuracy))
    print("Precisao: {}".format(precision))
    print("Sensibilidade: {}".format(recall))
    print("Especificidade: {}".format(specificity))
    print("False Positive Rate: {}".format(fpr))
    print("F-score: {}".format(fscore))

    print("____________________________________________\n")

    with open('resultados/' + str(pctBase) + '/' + str(numeroTeste) + '/teste_'+ str(pctBase) + '_0' + str(numeroTeste) + '.txt', 'w', encoding="utf-8") as results:

        results.write("Executado em: {} \n".format(date.today()))
        results.write("Pessoas procuradas: {} \n".format(pessoasConhecidas))

        results.write("Pessoas desconhecidas: {} \n".format(pessoasDesconhecidas))

        results.write("Total de Imagens: {}".format(totalImagens))
        results.write("\n")

        results.write("Total de faces detectadas: {}".format(totalFaces))
        results.write("\n")

        results.write("Total de faces reconhecidas: {}".format(facesReconhecidas))
        results.write("\n")

        results.write("\n")
        results.write("\n")
        results.write("Lista VP [{}]: {}".format(len(listaVP), listaVP))
        results.write("\n")

        results.write("Lista VN [{}]: {}".format(len(listaVN), listaVN))
        results.write("\n")

        results.write("Lista FP [{}]: {}".format(len(listaFP), listaFP))
        results.write("\n")

        results.write("Lista FN [{}]: {}".format(len(listaFN), listaFN))
        results.write("\n")
        results.write("\n")

        results.write("Matrix Confusion Results\n")
        results.write("Acuracia: {}\n".format(accuracy))
        results.write("Precisao: {}\n".format(precision))
        results.write("Sensibilidade: {}\n".format(recall))
        results.write("Especificidade: {}\n".format(specificity))
        results.write("False Positive Rate: {}\n".format(fpr))
        results.write("F-score: {}\n".format(fscore))

        end = datetime.datetime.now()

        results.write("Tempo de execução: \n {}".format(end - start))

    results.close
    cv2.destroyAllWindows()
    numeroTeste += 1

