import cv2
import os
import glob

exclude = []  # irá armazenar as fotos com pior qualidade de iluminação para no fim exclui-las
trshd = 37  # limiar para a classificacao de uma foto escura (pode ser modificada dinamicamente), quanto mais alto, maior a claridade da imagem
qtdImagensExlusao = 2415

def diminuirBanco():
    print("Iniciando Exclusao ... ")
    print("Serão removidas {} imagens com baixa iluminação".format(len(exclude)))
    file = ' '
    for i in range(qtdImagensExlusao):
        file = exclude[i]
        print("Removendo Arquivo: {}".format(file))
        os.remove(os.path.join(file))
    print()


while (len(exclude) != qtdImagensExlusao):
    print("Operacao trshd {} ... \n \n \n \n \n".format(trshd))
    for arquivo in glob.glob(os.path.join('yalefaces25/**/*.pgm')):

        if len(exclude) == qtdImagensExlusao:
            break

        # ------------------------Reading image--------------------------------------
        img = cv2.imread(arquivo)
        img_dot = img
        faceAtual = arquivo.split("\\")[2].split(".")[0]

        # -----Converting image to LAB Color model-----------------------------------
        lab = cv2.cvtColor(img, cv2.COLOR_BGR2LAB)

        # -----Splitting the LAB image to different channels-------------------------
        l, a, b = cv2.split(lab)

        # -----Finding average lightness level in image by fixing some points--------
        y, x, z = img.shape  # height, width of image
        print('>> Dimensao da Imagem => X:{}, Y:{}'.format(x, y))
        # Now we will decide some dynamic points on image for checking light intensity
        l_blur = cv2.GaussianBlur(l, (11, 11), 5)
        maxval = []
        count_percent = 3  # percent of total image
        count_percent = count_percent / 100
        row_percent = int(count_percent * x)  # 1% of total pixels widthwise
        column_percent = int(count_percent * y)  # 1% of total pizel height wise
        for i in range(1, x - 1):
            if i % row_percent == 0:
                for j in range(1, y - 1):
                    if j % column_percent == 0:
                        pix_cord = (i, j)

                        cv2.circle(img_dot, (int(i), int(j)), 5, (0, 255, 0), 2)
                        img_segment = l_blur[i:i + 3, j:j + 3]
                        (minVal, maxVal, minLoc, maxLoc) = cv2.minMaxLoc(img_segment)
                        maxval.append(maxVal)

        avg_maxval = round(sum(maxval) / len(maxval))

        print('>> Face Atual: {}'.format(faceAtual))
        print('>> Pontos: {}'.format(len(maxval)))
        print('>> Media de brilho: {}'.format(avg_maxval))

        print("Vetor Exclude: {} \ntrshd {}".format(len(exclude), trshd))

        if avg_maxval <= trshd:
            exclude.append(arquivo)
            exclude = sorted(set(exclude))
            print("A seguinte imagem foi adicionada ao vetor Exclude {} com o trshd de {}".format(faceAtual, trshd))

    # a variavel trshd é incrementada em +1 até que o vetor exclude fique cheio (com o valor exato para diminuir base)
    #com isso uma varredura no banco é efetuada diversas vezes até excluir X imagens com piores condições de iluminação
    if len(exclude) == qtdImagensExlusao:
        print("Vetor Exclude Final: {} \ntrshd {}".format(len(exclude), trshd))
        diminuirBanco()  # metodo para diminuir a base
        # linha para salvar quais imagens que foram excluidas
        with open('resultados/imagens_excluidas/25/exclude.txt', 'w', encoding="utf-8") as results:
            results.write("Imagem Excluida: {}\n".format(exclude))
            results.write("Threshold Final: {}\n".format(trshd))
        results.close()
        break
    else:
        trshd += 1

