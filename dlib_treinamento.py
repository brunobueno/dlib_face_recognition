import os
import glob
import _pickle as cPickle
import dlib
import cv2
import numpy as np

detectorFace = dlib.get_frontal_face_detector() #metodo para detectar faces com bounding boxes
detectorPontosFaciais = dlib.shape_predictor("recursos/shape_predictor_68_face_landmarks.dat") #metodo para detectar os 68 pontos faciais do rosto humano, passando como parametro o arquivo treinando em HOG e SVM
reconhecimentoFacial = dlib.face_recognition_model_v1("recursos/dlib_face_recognition_resnet_model_v1.dat") #treinamento feito especificamente para reconhecimento facial com rede convolucional

indice = {}
idx = 0
descritoresFaciais = None


#for para perccorrer imagens de treinamento somente com extensões JPG
#os.path.join especificar o caminho dos arquivos
#esse for vai extrair o descritorFacial de cada uma dessas imagens (128 caracteristicas que o algoritmo achou mais importante de cada uma delas)
for arquivo in glob.glob(os.path.join("treinamento/imagens/25/10/conhecidos/**/*.pgm")):

    imagem = cv2.imread(arquivo) #para ler a imagem
    facesDetectadas = detectorFace(imagem, 2)


    numeroFacesDetectadas = len(facesDetectadas) #para detectar outras faces
    #print(numeroFacesDetectadas)

    #validação para encontrar uma face ou mais de uma face
    #IMPORTANTE: quando for fazer o treinamento, obrigatoriamente deve ter somente 1 face na imagem, se não o algoritmo se perde
    if numeroFacesDetectadas > 1:
        print("Há mais de uma face na imagem {}".format(arquivo))
        exit(0)
    elif numeroFacesDetectadas < 1:
        print("Nenhuma face detectada no arquivo".format(arquivo))
        exit(0)

    for face in facesDetectadas:
        pontosFaciais = detectorPontosFaciais(imagem, face) #a variavel face armazena o recorte do BBOX, a variavel imagem possui a imagem toda
        #aqui utilizamos o metodo detectorPontosFaciais passando como parametro a imagem inteira e da imagem inteira vamos detectar os pontos da varial face, que eh a regiao de interesse

        #treinamento é criar um descritor facial
        # reconhecimentoFacial.compute_face_descriptor - vai computador quais são as principais caracteristicas que existem na face
        descritorFacial = reconhecimentoFacial.compute_face_descriptor(imagem, pontosFaciais)
        #o resultado do descritor facial será um vetor com 128 posições que irá descrever a face que foi encontrada
        #sim, isso é aplicação dos kernels ou filtros, para escolher a melhor caracteristica da imagem
        # print(format(arquivo))
         #print(len(descritorFacial))
         #print(descritorFacial) #printa todas as caracteristicas principais, que são efetivamente o treinamento
        #a rede neural encontrar as melhores caracteristicas
        # vc nao precisa de preocupar com isso

        print(descritorFacial)
        listaDescritorFacial = [df for df in descritorFacial] #para converter o descritor de face no formato do Dlib para uma lista com o tamanho de 128, pois precisamos gravar um arquivo com esses dados
        print(listaDescritorFacial)

        #aqui precisamos converter o vetor em um array do tipo NUMPY
        npArrayDescritorFacial = np.asarray(listaDescritorFacial, dtype=np.float64)
        print(npArrayDescritorFacial)

        #precisamos aumentar uma dimensão
        npArrayDescritorFacial = npArrayDescritorFacial[np.newaxis, :]
        print(npArrayDescritorFacial)
        #np.newaxis cria uma nova coluna


        if descritoresFaciais is None:
            descritoresFaciais = npArrayDescritorFacial
        else:
            descritoresFaciais = np.concatenate((descritoresFaciais, npArrayDescritorFacial), axis=0)
            #A função de concatenação pode ter duas ou mais matrizes da mesma forma e, por padrão, concatena em linha, ou seja, eixo = 0
        indice[idx] = arquivo
        print(indice)
        idx += 1


#     cv2.imshow("treinamento", imagem)
#     cv2.waitKey(0)


#print("Tamanho: {} Formato: {}".format(len(descritoresFaciais), descritoresFaciais.shape))
#o resultado foi uma matriz de 8 linhas (fotos) com 128 colunas (caracteristicas)

#print(indice)
#a variavel indice é um vinculo entre as 128 caracteristicas da imagem junto com o NOME da foto


#para gravar o arquivo de descritores dessas imagens (as principais caracteristcas de cada imagem)
np.save("treinamento/descritores/25/10/descritores.npy", descritoresFaciais)

#arquivo indice que apresenta o id e o nome de cada imagem
with open("treinamento/indices/25/10/indices.pickle", 'wb') as f:
    cPickle.dump(indice, f)


# cv2.destroyAllWindows()
